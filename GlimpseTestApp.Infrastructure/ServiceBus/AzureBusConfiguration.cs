namespace GlimpseTestApp.Infrastructure.ServiceBus
{
    public class AzureBusConfiguration
    {
        public string ConnectionString { get; set; }
        public string Queue { get; set; }
    }
}