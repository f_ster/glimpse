﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GlimpseTestApp.Core.Domain;
using GlimpseTestApp.Core.Interfaces;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;

namespace GlimpseTestApp.Infrastructure.ServiceBus
{
    public class BusProvider : IBusProvider
    {
        private readonly IQueueClient _queueClient;
        private Func<Image, Task<bool>> _messageHandler;

        public BusProvider(AzureBusConfiguration configuration)
        {
            _queueClient = new QueueClient(configuration.ConnectionString, configuration.Queue);
        }
        public void StartListener(Func<Image, Task<bool>> handler)
        {
            _messageHandler = handler;
            _queueClient.RegisterMessageHandler(Handler, new MessageHandlerOptions(ExceptionReceivedHandler)
            {
                AutoComplete = false,
                MaxConcurrentCalls = 50
            });
        }

        private Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            Console.WriteLine($"Message handler encountered an exception {exceptionReceivedEventArgs.Exception}.");
            var context = exceptionReceivedEventArgs.ExceptionReceivedContext;
            Console.WriteLine("Exception context for troubleshooting:");
            Console.WriteLine($"- Endpoint: {context.Endpoint}");
            Console.WriteLine($"- Entity Path: {context.EntityPath}");
            Console.WriteLine($"- Executing Action: {context.Action}");
            return Task.CompletedTask;
        }

        private async Task Handler(Message message, CancellationToken cancellationToken)
        {
            var image = JsonConvert.DeserializeObject<Image>(Encoding.UTF8.GetString(message.Body));
            var result = await _messageHandler(image);
            await (result
                ? _queueClient.CompleteAsync(message.SystemProperties.LockToken)
                : _queueClient.AbandonAsync(message.SystemProperties.LockToken));
        }

        public Task Send(Image image)
        {
            var serialized = JsonConvert.SerializeObject(image);
            return _queueClient.SendAsync(new Message(Encoding.UTF8.GetBytes(serialized)));
        }
    }
}
