using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using GlimpseTestApp.Core.Domain;
using GlimpseTestApp.Core.Interfaces;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Cosmos.Linq;

namespace GlimpseTestApp.Infrastructure.CosmosDb
{
    public class Repository : IRepository
    {
        private readonly CosmosClient _cosmosClient;
        private readonly CosmosConfiguration _cosmosConfiguration;
        private ChangeFeedProcessor _changeFeedProcessor;

        public Repository(CosmosConfiguration cosmosConfiguration)
        {
            _cosmosClient = new CosmosClient(cosmosConfiguration.ConnectionString);
            _cosmosConfiguration = cosmosConfiguration;
        }

        public async Task<bool> SaveAsync<T>(T item)
        {
            var container = _cosmosClient.GetContainer(_cosmosConfiguration.DatabaseName, CosmosExtensions.GetCollectionName<T>());
            var response = await container.UpsertItemAsync(item);
            return response.StatusCode == HttpStatusCode.Created;
        }

        public async Task<bool> SaveAsync<T>(IEnumerable<T> items)
        {
            var container = _cosmosClient.GetContainer(_cosmosConfiguration.DatabaseName, CosmosExtensions.GetCollectionName<T>());

            var tasks = items.Select(x => container.UpsertItemAsync(x)).ToArray();
            await Task.WhenAll(tasks);

            return tasks.All(x => x.Result.StatusCode == HttpStatusCode.Created);
        }

        public void AddEnumerator<T>(Func<IReadOnlyCollection<T>, CancellationToken, Task> handler, string hostName)
        {
            var leaseContainer = _cosmosClient.GetContainer(_cosmosConfiguration.DatabaseName, _cosmosConfiguration.LeasesCollectionName);
            _changeFeedProcessor = _cosmosClient.GetContainer(_cosmosConfiguration.DatabaseName, CosmosExtensions.GetCollectionName<T>())
                .GetChangeFeedProcessorBuilder<T>("statisticsEnumerator", (changes, token) => handler(changes, token))
                .WithInstanceName(hostName)
                .WithLeaseContainer(leaseContainer)
                //TODO tweak start date
                .WithStartTime(DateTime.Today.AddDays(-2))
                .Build();
        }

        public Task StartEnumerationAsync()
        {
            return _changeFeedProcessor.StartAsync();
        }

        public async Task<List<T>> SelectWhere<T>(Expression<Func<T, bool>> expression)
        {
            var container = _cosmosClient.GetContainer(_cosmosConfiguration.DatabaseName, CosmosExtensions.GetCollectionName<T>());
            var feedIterator = container.GetItemLinqQueryable<T>()
                .Where(expression)
                .ToFeedIterator();

            var results = new List<T>();
            while (feedIterator.HasMoreResults)
            {
                //NOTE: cosmos sdk throws on 404 workaround :(
                try
                {
                    var next = await feedIterator.ReadNextAsync();
                    results.AddRange(next);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    break;
                }
            }

            return results;
        }

        public async Task<List<T>> SelectAllAsync<T>(int skip, int take)
        {
            var container = _cosmosClient.GetContainer(_cosmosConfiguration.DatabaseName, CosmosExtensions.GetCollectionName<T>());
            var feedIterator = container.GetItemLinqQueryable<T>()
                .Skip(skip)
                .Take(take)
                .ToFeedIterator();

            var results = new List<T>();
            while (feedIterator.HasMoreResults)
            {
                var next = await feedIterator.ReadNextAsync();
                results.AddRange(next);
            }

            return results;
        }

        public async Task Migrate(params Type[] collections)
        {
            var databaseIfNotExistsAsync = await _cosmosClient.CreateDatabaseIfNotExistsAsync(_cosmosConfiguration.DatabaseName);
            await databaseIfNotExistsAsync.Database.CreateContainerIfNotExistsAsync(_cosmosConfiguration.LeasesCollectionName, "/id");
            await Task.WhenAll(collections.Select(x =>
                databaseIfNotExistsAsync.Database.CreateContainerIfNotExistsAsync(CosmosExtensions.GetCollectionName(x),
                    x == typeof(HourlyTotal) ? "/Tag" : "/id")));
        }
    }
}
