using System;
using GlimpseTestApp.Core.Domain;

namespace GlimpseTestApp.Infrastructure.CosmosDb
{
    public static class CosmosExtensions
    {
        public static string GetCollectionName<T>() => GetCollectionName(typeof(T));

        public static string GetCollectionName(Type type)
        {
            return type.Name switch
            {
                nameof(Image) => "images",
                nameof(Tag) => "tags",
                nameof(HourlyTotal) => "hourlyTotals",
                _ => throw new ArgumentOutOfRangeException($"No collection specified for {type.Name}")
            };
        }
    }
}
