namespace GlimpseTestApp.Infrastructure.CosmosDb
{
    public class CosmosConfiguration
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string LeasesCollectionName { get; set; }
    }
}
