using System;
using System.IO;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using GlimpseTestApp.Core.Domain;
using GlimpseTestApp.Core.Interfaces;

namespace GlimpseTestApp.Infrastructure.BlobStorage
{
    public class StorageService : IStorageService
    {
        private readonly BlobContainerClient _containerClient;

        public StorageService(BlobContainerConfig config)
        {
            _containerClient = new BlobContainerClient(config.ConnectionString, config.ContainerName);
        }
        public async Task<Stream> ReadBlob(string name)
        {
            var response = await _containerClient.GetBlobClient(name).DownloadAsync();
            return response.Value.Content;
        }

        public async Task SaveBlob(FileModel file)
        {
            await using var memoryStream = new MemoryStream(Convert.FromBase64String(file.Content));
            await _containerClient.UploadBlobAsync(file.Name, memoryStream);
        }
    }
}
