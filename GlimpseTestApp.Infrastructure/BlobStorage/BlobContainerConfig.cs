namespace GlimpseTestApp.Infrastructure.BlobStorage
{
    public class BlobContainerConfig
    {
        public string ConnectionString { get; set; }
        public string ContainerName { get; set; }
    }
}
