import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {IAppState} from "../store/state/app.state";
import {select, Store} from "@ngrx/store";
import {GetPie} from "../store/actions/pie.actions";
import {selectTagColors, selectTagNames, selectTagsCount} from "../store/selectors/pie.selector";
import {ChartOptions, ChartType} from "chart.js";
import {Color, Label} from "ng2-charts";
import {GetAllHourly, GetHourlyByTag} from "../store/actions/hourly.actions";
import {Observable} from "rxjs";

@Component({
  selector: 'app-pie-component',
  templateUrl: './pie-chart.component.html'
})
export class PieChartComponent implements OnInit{

  constructor(private _store: Store<IAppState>, private _router: Router) {
    this.pieChartData = this._store.pipe(select(selectTagsCount));
    this.pieChartLabels = this._store.pipe(select(selectTagNames));
    this.pieChartColors = this._store.pipe(select(selectTagColors));
  }

  ngOnInit(): void {
    this._store.dispatch(new GetPie());
  }

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartLabels: Observable<Label[]>;
  public pieChartData: Observable<number[]>;
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartColors:Observable<Color[]>;

  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    let a:any = active[0];
    if (a === undefined)
      return;
    const {label} = a._model;
    this._store.dispatch(new GetHourlyByTag(label))
  }

  public refreshClicked(){
    this._store.dispatch(new GetPie());
    this._store.dispatch(new GetAllHourly());
  }
}
