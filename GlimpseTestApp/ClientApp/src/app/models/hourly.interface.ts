import {ITag} from "./tag.interface";

export interface ITagHourly {
  hour: number,
  stats: ITag[]
}
