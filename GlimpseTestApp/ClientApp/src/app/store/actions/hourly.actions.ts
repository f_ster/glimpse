import {Action} from "@ngrx/store";
import {ITagHourly} from "../../models/hourly.interface";

export enum EHourlyActions {
  GetAllHourly = '[Hourly] Get All Hourly',
  GetAllHourlySuccess = '[Hourly] Get Hourly chart Success',
  GetHourlyByTag = '[Hourly] Get Hourly By Tag',
  GetHourlyByTagSuccess = '[Hourly] Get Hourly By Tag Success'
}

export class GetAllHourly implements Action{
  type = EHourlyActions.GetAllHourly;
}

export class GetAllHourlySuccess implements Action {
  type = EHourlyActions.GetAllHourlySuccess;

  constructor(public payload: ITagHourly[]) {}
}

export class GetHourlyByTag implements Action {
  type = EHourlyActions.GetHourlyByTag;

  constructor(public payload: string) {}
}

export class GetHourlyByTagSuccess implements Action {
  type = EHourlyActions.GetHourlyByTagSuccess;

  constructor(public payload: ITagHourly[]) {}
}

export type HourlyActions = GetAllHourly | GetAllHourlySuccess | GetHourlyByTag | GetHourlyByTagSuccess;
