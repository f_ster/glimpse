import {Action} from "@ngrx/store";
import {ITag} from "../../models/tag.interface";

export enum EPieActions {
  GetPie = '[Pie] Get Pie chart',
  GetPieSuccess = '[Pie] Get Pie chart Success',
  GetPiePiece = '[Pie] Get Pie chart segment',
  GetPiePieceSuccess = '[Pie] Get Pie chart segment Success'
}

export class GetPie implements Action{
  type = EPieActions.GetPie;
}

export class GetPieSuccess implements Action {
  type = EPieActions.GetPieSuccess;

  constructor(public payload: ITag[]) {}
}

export class GetPiePiece implements Action {
  type = EPieActions.GetPiePiece;

  constructor(public payload: string) {}
}

export class GetPiePieceSuccess implements Action {
  type = EPieActions.GetPiePieceSuccess;

  constructor(public payload: ITag) {}
}

export type PieActions = GetPie | GetPieSuccess | GetPiePiece | GetPiePieceSuccess;
