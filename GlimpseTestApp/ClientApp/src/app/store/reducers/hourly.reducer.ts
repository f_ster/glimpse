import {IHourlyState, initialHourlyState} from "../state/hourly.state";
import {EHourlyActions, GetAllHourlySuccess, GetHourlyByTagSuccess, HourlyActions} from "../actions/hourly.actions";

export const hourlyReducer = (
  state = initialHourlyState,
  action: HourlyActions
): IHourlyState => {
  switch (action.type) {
    case EHourlyActions.GetAllHourlySuccess:
    case EHourlyActions.GetHourlyByTagSuccess: {
      return {
        ...state,
        tags: (action as GetHourlyByTagSuccess).payload
      };
    }

    default:
      return state;
  }
};
