import {initialPieState, IPieState} from "../state/pie.state";
import {EPieActions, GetPiePieceSuccess, GetPieSuccess, PieActions} from "../actions/pie.actions";

export const pieReducer = (
  state = initialPieState,
  action: PieActions
): IPieState => {
  switch (action.type) {
    case EPieActions.GetPieSuccess: {
      return {
        ...state,
        tags: (action as GetPieSuccess).payload
      };
    }
    case EPieActions.GetPiePieceSuccess: {
      return {
        ...state,
        selectedTag: (action as GetPiePieceSuccess).payload
      };
    }

    default:
      return state;
  }
};
