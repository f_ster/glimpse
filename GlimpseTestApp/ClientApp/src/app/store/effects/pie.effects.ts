import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { Store} from '@ngrx/store';
import { of } from 'rxjs';
import { switchMap} from 'rxjs/operators';

import { IAppState } from '../state/app.state';
import {
  EPieActions,
  GetPie,
  GetPieSuccess
} from '../actions/pie.actions';

import {TagService} from "../../services/tag.service";
import {ITag} from "../../models/tag.interface";

@Injectable()
export class PieEffects {
  @Effect()
  getPiePiece$ = this._actions$.pipe(
    ofType<GetPie>(EPieActions.GetPie),
    switchMap(() => this._tagService.getTags()),
    switchMap((tags: ITag[]) => of(new GetPieSuccess(tags)))
  );

  constructor(
    private _tagService: TagService,
    private _actions$: Actions,
    private _store: Store<IAppState>
  ) {}
}
