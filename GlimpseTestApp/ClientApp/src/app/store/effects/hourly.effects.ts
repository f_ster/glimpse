import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { Store} from '@ngrx/store';
import { of } from 'rxjs';
import { switchMap} from 'rxjs/operators';

import { IAppState } from '../state/app.state';
import {
  EHourlyActions,
  GetAllHourly,
  GetAllHourlySuccess,
  GetHourlyByTag,
  GetHourlyByTagSuccess
} from '../actions/hourly.actions';

import {TagService} from "../../services/tag.service";
import {ITagHourly} from "../../models/hourly.interface";

@Injectable()
export class HourlyEffects {
  @Effect()
  getPie$ = this._actions$.pipe(
    ofType<GetHourlyByTag>(EHourlyActions.GetHourlyByTag),
    switchMap(x=>this._tagService.getHourlyByTag(x.payload)),
    switchMap((tags:ITagHourly[]) => of(new GetHourlyByTagSuccess(tags)))
  );

  @Effect()
  getAllHourly = this._actions$.pipe(
    ofType<GetAllHourly>(EHourlyActions.GetAllHourly),
    switchMap(() => this._tagService.getHourly()),
    switchMap((tags: ITagHourly[]) => of(new GetAllHourlySuccess(tags)))
  );

  constructor(
    private _tagService: TagService,
    private _actions$: Actions,
    private _store: Store<IAppState>
  ) {}
}
