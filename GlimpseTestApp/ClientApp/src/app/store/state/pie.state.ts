import {ITag} from "../../models/tag.interface";

export interface IPieState {
 tags: ITag[];
 selectedTag: ITag;
}

export const initialPieState: IPieState = {
  tags: [{tag: "No Data", count: 0}],
  selectedTag: null
}
