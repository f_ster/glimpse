import {ITagHourly} from "../../models/hourly.interface";

export interface IHourlyState {
  tags: ITagHourly[]
}

export const initialHourlyState: IHourlyState = {
  tags: [
    {hour: 0, stats: [{tag: "No Data", count: 0}]}
  ]
}
