import {initialPieState, IPieState} from "./pie.state";
import {initialHourlyState, IHourlyState} from "./hourly.state";

export interface IAppState {
  pie: IPieState;
  hourly: IHourlyState;
}

export const initialAppState: IAppState = {
  pie: initialPieState,
  hourly: initialHourlyState
}

export function getInitialState(): IAppState {
  return initialAppState;
}
