import {createSelector} from "@ngrx/store";
import {IAppState} from "../state/app.state";
import {IHourlyState} from "../state/hourly.state";
import {ChartDataSets} from "chart.js";
import * as stc from "string-to-color";

const selectHourly = (state: IAppState) => state.hourly;

export const selectAllTags = createSelector(selectHourly, (state: IHourlyState) => state.tags.map(x=> x.hour.toString()));
export const selectTagHorlyData = createSelector(selectHourly, (state: IHourlyState) => {
  let barData:ChartDataSets[] = [];
  for (const hourlyStat of state.tags) {
    for (const tag of hourlyStat.stats) {
      let existed = barData.find(x => x.label === tag.tag)
      if (existed === undefined)
        barData.push({
          data: [tag.count],
          label: tag.tag,
          hoverBackgroundColor: stc(tag.tag),
          backgroundColor: stc(tag.tag)
        });
      else existed.data.push(tag.count);
    }
  }

  return barData;
});
