import {createSelector} from "@ngrx/store";
import {IAppState} from "../state/app.state";
import {IPieState} from "../state/pie.state";
import * as stc from "string-to-color";

const selectPie = (state: IAppState) => state.pie;

export const selectTagNames = createSelector(selectPie, (state: IPieState) => state.tags.map(x=>x.tag));
export const selectTagsCount = createSelector(selectPie, (state: IPieState) => state.tags.map(x=>x.count));
export const selectTagColors = createSelector(selectPie,
  (state: IPieState) => [
    {backgroundColor: state.tags.map(x => stc(x.tag))}
    ]);
