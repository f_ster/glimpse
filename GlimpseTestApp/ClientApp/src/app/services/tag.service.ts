import {HttpClient} from "@angular/common/http";
import {Inject} from "@angular/core";
import {Observable} from "rxjs";
import {ITag} from "../models/tag.interface";
import {ITagHourly} from "../models/hourly.interface";

export class TagService {
  private readonly _baseUrl: string;

  constructor(private _httpClient: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this._baseUrl = baseUrl;
  }

  getTags(): Observable<ITag[]> {
    return this._httpClient.get<ITag[]>(this._baseUrl + 'api/Statistics/pieData');
  }
  getHourly(): Observable<ITagHourly[]> {
    return this._httpClient.get<ITagHourly[]>(this._baseUrl + 'api/Statistics/hourly');
  }
  getHourlyByTag(tag: string): Observable<ITagHourly[]> {
    return this._httpClient.get<ITagHourly[]>(this._baseUrl + 'api/Statistics/hourly/' + tag);
  }
}
