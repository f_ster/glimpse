import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import {select, Store} from "@ngrx/store";
import {IAppState} from "../store/state/app.state";
import {selectAllTags, selectTagHorlyData} from '../store/selectors/hourly.selector';
import {GetAllHourly} from "../store/actions/hourly.actions";
import {Observable} from "rxjs";

@Component({
  selector: 'app-bar-chart',
  templateUrl: './barchart.component.html',
})
export class BarChartComponent implements OnInit {
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{stacked: true}], yAxes: [{stacked: true}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Observable<Label[]>;
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  public barChartData: Observable<ChartDataSets[]>;

  constructor(private _store: Store<IAppState>) {
    this.barChartData = this._store.pipe(select(selectTagHorlyData));
    this.barChartLabels = this._store.pipe(select(selectAllTags));
  }

  ngOnInit() {
    this._store.dispatch(new GetAllHourly());
  }
}
