import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { PieChartComponent } from './charts/pie-chart.component';
import {rootReducer} from "./root.reducer";
import {TagService} from "./services/tag.service";
import {EffectsModule} from "@ngrx/effects";
import {PieEffects} from "./store/effects/pie.effects";
import {ChartsModule} from "ng2-charts";
import {BarChartComponent} from "./barchart/barchart.component";
import {HourlyEffects} from "./store/effects/hourly.effects";

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    BarChartComponent,
    PieChartComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    StoreModule.forRoot(rootReducer),
    EffectsModule.forRoot([PieEffects, HourlyEffects]),
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'charts', component: PieChartComponent },
    ]),
    ChartsModule
  ],
  providers: [TagService],
  bootstrap: [AppComponent]
})
export class AppModule { }
