import {ActionReducerMap} from "@ngrx/store";
import {IAppState} from "./store/state/app.state";
import {pieReducer} from "./store/reducers/pie.reducer";
import {hourlyReducer} from "./store/reducers/hourly.reducer";

export const rootReducer: ActionReducerMap<IAppState, any> ={
  pie: pieReducer,
  hourly: hourlyReducer
}
