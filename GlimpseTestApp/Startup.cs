using System;
using System.Threading.Tasks;
using GlimpseTestApp.Configuration;
using GlimpseTestApp.Core.Domain;
using GlimpseTestApp.Core.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace GlimpseTestApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.ConfigureProduction(Configuration);
            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration => { configuration.RootPath = "ClientApp/dist"; });
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Description = "Test",
                    Title = "Test",
                    Version = "v1"
                });
                options.DescribeAllEnumsAsStrings();
                options.AddSecurityDefinition("basic", new OpenApiSecurityScheme());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime hostApplicationLifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.RoutePrefix = "swagger";
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "ML");
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });

            hostApplicationLifetime.ApplicationStarted.Register(async () => await AppStarted(app.ApplicationServices));
        }

        private async Task AppStarted(IServiceProvider serviceProvider)
        {
            var busProvider = serviceProvider.GetService<IBusProvider>();
            var imageProcessingService = serviceProvider.GetService<IImageProcessingCommandService>();
            var repository = serviceProvider.GetService<IRepository>();

            await repository.Migrate(typeof(Image), typeof(Tag), typeof(HourlyTotal));

            busProvider.StartListener(imageProcessingService.Process);
            repository.AddEnumerator<Image>(imageProcessingService.ActualizeStatistics, Environment.MachineName);
            await repository.StartEnumerationAsync();
        }
    }
}
