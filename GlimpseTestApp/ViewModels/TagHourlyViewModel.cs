namespace GlimpseTestApp.ViewModels
{
    public class TagHourlyViewModel
    {
        public int Hour { get; set; }
        public TagViewModel[] Stats { get; set; }
    }
}