using GlimpseTestApp.Core.Domain;

namespace GlimpseTestApp.ViewModels
{
    public class TagViewModel
    {
        public string Tag { get; set; }
        public int Count { get; set; }

        public static TagViewModel FromTag(Tag tag)
        {
            return new TagViewModel
            {
                Tag = tag.Name,
                Count = tag.PredictionsCount
            };
        }
    }
}