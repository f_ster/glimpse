using System.Threading.Tasks;
using GlimpseTestApp.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace GlimpseTestApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ImageController : ControllerBase
    {
        private readonly IImageProcessingCommandService _imageProcessingCommandService;

        public ImageController(IImageProcessingCommandService imageProcessingCommandService)
        {
            _imageProcessingCommandService = imageProcessingCommandService;
        }

        [HttpPost]
        public async Task<IActionResult> Upload(string content)
        {
            await _imageProcessingCommandService.Upload(content);
            return Ok();
        }
    }
}
