﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GlimpseTestApp.Core.Interfaces;
using GlimpseTestApp.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace GlimpseTestApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StatisticsController : ControllerBase
    {
        private readonly IImageProcessingQueryService _queryService;

        public StatisticsController(IImageProcessingQueryService queryService)
        {
            _queryService = queryService;
        }

        [HttpGet("pieData")]
        public async Task<IEnumerable<TagViewModel>> GetTags()
        {
            var tags = await _queryService.SelectTagStatisticsAsync();
            //TODO automapper
            return tags.Select(TagViewModel.FromTag);
        }

        [HttpGet("hourly")]
        public async Task<IEnumerable<TagHourlyViewModel>> GetAllHourlyStats()
        {
            var statisticsAsync = await _queryService.SelectHourlyStatisticsAsync();
            return statisticsAsync
                .Select(x => new TagHourlyViewModel
                {
                    Hour = x.Hour,
                    Stats = x.Tags.Select(t => new TagViewModel {Tag = t.Name, Count = t.Count}).ToArray()
                });
        }

        [HttpGet("hourly/{tag}")]
        public async Task<IEnumerable<TagHourlyViewModel>> GetHourlyStats(string tag)
        {
            var statisticsAsync = await _queryService.SelectHourlyStatisticsAsync(tag);
            return statisticsAsync
                .Select(x => new TagHourlyViewModel
                {
                    Hour = x.Hour,
                    Stats = x.Tags.Select(t => new TagViewModel{Tag = t.Name, Count = t.Count}).ToArray()
                });
        }
    }
}
