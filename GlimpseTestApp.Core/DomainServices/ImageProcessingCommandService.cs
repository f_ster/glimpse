using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GlimpseTestApp.Core.Domain;
using GlimpseTestApp.Core.Interfaces;

namespace GlimpseTestApp.Core.DomainServices
{
    public class ImageProcessingCommandService : IImageProcessingCommandService
    {
        private readonly IPredictionService _predictionService;
        private readonly IStorageService _storageService;
        private readonly IRepository _repository;
        private readonly IBusProvider _busProvider;

        public ImageProcessingCommandService(
            IPredictionService predictionService,
            IStorageService storageService,
            IRepository repository,
            IBusProvider busProvider
            )
        {
            _predictionService = predictionService;
            _storageService = storageService;
            _repository = repository;
            _busProvider = busProvider;
        }

        public async Task<bool> Process(Image image)
        {
            Result<string[]> predict;
            await using (var stream = await _storageService.ReadBlob(image.Id.ToString()).ConfigureAwait(false))
            {
                predict = _predictionService.Predict(stream);
            }

            if (!predict.IsSuccessful)
            {
                return false;
            }

            image.ApplyTags(predict.Data);
            await _repository.SaveAsync(image).ConfigureAwait(false);
            return true;
        }

        public async Task Upload(string content)
        {
            //todo use date from image/client
            var image = new Image(DateTimeOffset.UtcNow);
            await _storageService.SaveBlob(new FileModel(image.Id.ToString(), content));
            await _busProvider.Send(image);
        }

        public async Task ActualizeStatistics(IReadOnlyCollection<Image> images, CancellationToken cancellationToken)
        {
            var tagNames = images.SelectMany(x => x.Tags).Distinct().ToArray();
            var existedTags = await _repository.SelectWhere<Tag>(x => tagNames.Contains(x.Name));
            var existedHourlyTotals = await _repository.SelectWhere<HourlyTotal>(x => tagNames.Contains(x.Tag));
            var tags = tagNames
                .Where(x => existedTags.All(t => t.Name != x))
                .Select(x => new Tag(x))
                .Concat(existedTags)
                .ToArray();
            var hourlyTotals = tagNames
                .Where(x => existedHourlyTotals.All(t => t.Tag != x))
                .SelectMany(x => Enumerable.Range(0, 24).Select(t=>new HourlyTotal(x, t)))
                .Concat(existedHourlyTotals)
                .ToArray();

            // var tagsToSave = images.SelectMany(x => x.Tags
            //         .Select(t => (Tag: t, Image: x))
            //         .GroupBy(t => t.Tag)
            //         .Join(tags, t => t.Key, t => t.Name,
            //             (imageForTag, tag) =>
            //             {
            //                 tag.PredictionsCount += imageForTag.Count();
            //                 var newTotals = imageForTag
            //                     .GroupBy(i => i.Image.DateCreated.Hour)
            //                     .Join(tag.HourlyTotals,
            //                         i => i.Key, ht => ht.Hour,
            //                         (hourly, total) =>
            //                         {
            //                             total.Count += hourly.Count();
            //                             return total;
            //                         })
            //                     .ToArray();
            //                 tag.HourlyTotals = newTotals;
            //                 return tag;
            //             })
            //     )
            //     .ToArray();

            foreach (var image in images)
            {
                foreach (var tag in image.Tags.Select(x => tags.First(t => t.Name == x)))
                {
                    tag.PredictionsCount += 1;
                    hourlyTotals.FirstOrDefault(x => x.Hour == image.DateCreated.Hour && x.Tag == tag.Name).Count += 1;
                }
            }

            await _repository.SaveAsync<Tag>(tags);
            await _repository.SaveAsync<HourlyTotal>(hourlyTotals);
        }

        public async Task GenerateStats()
        {
            var tagNames = new[]{"Bottle", "Glass", "Cocktail", "Beer", "Knife", "Bag", "Vodka", "Money", "Bible", "Tips"};
            var random = new Random();
            var tags = tagNames.Select(x => new Tag(x) {PredictionsCount = random.Next(20, 100)}).ToArray();
            await _repository.Migrate(typeof(Tag), typeof(HourlyTotal));
            await _repository.SaveAsync<Tag>(tags);
            var hourlyTotals = tags.SelectMany(x=> Enumerable.Range(0, 24).Select(h =>
            {
                var next = x.PredictionsCount / random.Next(3, 24);
                return new HourlyTotal(x.Name, h)
                {
                    Count = x.PredictionsCount -= next < 0 ? x.PredictionsCount : next
                };
            }));
            await _repository.SaveAsync(hourlyTotals);
        }
    }
}
