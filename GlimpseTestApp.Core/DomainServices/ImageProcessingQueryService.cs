using System.Linq;
using System.Threading.Tasks;
using GlimpseTestApp.Core.Domain;
using GlimpseTestApp.Core.Interfaces;

namespace GlimpseTestApp.Core.DomainServices
{
    public class ImageProcessingQueryService : IImageProcessingQueryService
    {
        private readonly IRepository _repository;

        public ImageProcessingQueryService(IRepository repository)
        {
            _repository = repository;
        }
        public async Task<Tag[]> SelectTagStatisticsAsync()
        {
            var tags = await _repository.SelectAllAsync<Tag>(0, 100);
            return tags.ToArray();
        }

        public async Task<TagHourly[]> SelectHourlyStatisticsAsync(string tag = null)
        {
            var hourlyTotals = await _repository
                .SelectWhere<HourlyTotal>(x => x.Hour >= 0 && (tag == null || x.Tag == tag));

            return hourlyTotals
                .GroupBy(x => x.Hour)
                .Select(x => new TagHourly
                {
                    Hour = x.Key,
                    Tags = x.Select(t => new TagHourly.Tag(t.Tag, t.Count)).ToArray()
                })
                .OrderBy(x => x.Hour)
                .ToArray();
        }
    }
}
