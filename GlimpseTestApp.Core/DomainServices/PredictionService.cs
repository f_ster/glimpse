using System;
using System.IO;
using System.Linq;
using System.Threading;
using GlimpseTestApp.Core.Domain;
using GlimpseTestApp.Core.Interfaces;

namespace GlimpseTestApp.Core.DomainServices
{
    public class PredictionService : IPredictionService
    {
        private const int ResourceCost = 5;
        private static volatile int _parallelWorkers;
        private static readonly string[] Tags = {
            "Bottle", "Glass", "Cocktail", "Beer", "Knife", "Bag", "Vodka", "Money", "Bible", "Tips"
        };

        public Result<string[]> Predict(Stream imageBinary)
        {
            Interlocked.Increment(ref _parallelWorkers);
            if (_parallelWorkers > ResourceCost)
            {
                var errorMessage = "time to increase replicas";
                Console.Error.WriteLine(errorMessage);
                return Result<string[]>.Fail(errorMessage);
            }

            Thread.Sleep(50);
            Interlocked.Decrement(ref _parallelWorkers);
            var random = new Random();
            var predictions = Tags
                .Skip(random.Next(Tags.Length - 1))
                .Take(random.Next(3))
                .Prepend(Tags[random.Next(Tags.Length)])
                .Distinct()
                .ToArray();
            return Result<string[]>.Success(predictions);
        }
    }
}
