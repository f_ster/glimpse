using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using GlimpseTestApp.Core.Domain;

namespace GlimpseTestApp.Core.Interfaces
{
    public interface IImageProcessingCommandService
    {
        Task<bool> Process(Image image);
        Task Upload(string content);
        Task ActualizeStatistics(IReadOnlyCollection<Image> images, CancellationToken cancellationToken);
        [Obsolete("migration like, use for testing only")]
        Task GenerateStats();
    }
}
