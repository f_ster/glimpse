using System.Threading.Tasks;
using GlimpseTestApp.Core.Domain;

namespace GlimpseTestApp.Core.Interfaces
{
    public interface IImageProcessingQueryService
    {
        Task<Tag[]> SelectTagStatisticsAsync();
        Task<TagHourly[]> SelectHourlyStatisticsAsync(string tag = null);
    }
}
