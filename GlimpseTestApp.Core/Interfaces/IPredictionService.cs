using System.IO;
using GlimpseTestApp.Core.Domain;

namespace GlimpseTestApp.Core.Interfaces
{
    public interface IPredictionService
    {
        Result<string[]> Predict(Stream imageBinary);
    }
}