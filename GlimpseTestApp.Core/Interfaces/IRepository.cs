using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace GlimpseTestApp.Core.Interfaces
{
    public interface IRepository
    {
        Task<bool> SaveAsync<T>(T item);
        Task<bool> SaveAsync<T>(IEnumerable<T> items);
        void AddEnumerator<T>(Func<IReadOnlyCollection<T>, CancellationToken, Task> handler, string hostName);
        Task StartEnumerationAsync();
        Task<List<T>> SelectWhere<T>(Expression<Func<T, bool>> expression);
        Task Migrate(params Type[] collections);
        Task<List<T>> SelectAllAsync<T>(int skip, int take);
    }
}
