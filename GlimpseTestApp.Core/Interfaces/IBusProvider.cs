using System;
using System.Threading.Tasks;
using GlimpseTestApp.Core.Domain;

namespace GlimpseTestApp.Core.Interfaces
{
    public interface IBusProvider
    {
        void StartListener(Func<Image, Task<bool>> handler);
        Task Send(Image image);
    }
}