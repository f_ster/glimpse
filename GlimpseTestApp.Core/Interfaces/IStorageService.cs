using System.IO;
using System.Threading.Tasks;
using GlimpseTestApp.Core.Domain;

namespace GlimpseTestApp.Core.Interfaces
{
    public interface IStorageService
    {
        Task<Stream> ReadBlob(string name);
        Task SaveBlob(FileModel file);
    }
}
