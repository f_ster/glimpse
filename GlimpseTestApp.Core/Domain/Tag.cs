using System;
using Newtonsoft.Json;

namespace GlimpseTestApp.Core.Domain
{
    public class Tag
    {
        [JsonProperty(PropertyName = "id")]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int PredictionsCount { get; set; }

        public Tag()
        {
            Id = Guid.NewGuid();
        }

        public Tag(string name) : this()
        {
            Name = name;
        }
    }
}
