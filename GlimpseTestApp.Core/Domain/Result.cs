namespace GlimpseTestApp.Core.Domain
{
    public class Result<T>
    {
        public bool IsSuccessful { get; set; }
        public T Data { get; set; }
        public string ErrorMessage { get; set; }

        public static Result<T> Success(T data = default)
        {
            return new Result<T>
            {
                IsSuccessful = true,
                Data = data
            };
        }

        public static Result<T> Fail(string errorMessage = "")
        {
            return new Result<T>
            {
                IsSuccessful = false,
                Data = default,
                ErrorMessage = errorMessage
            };
        }
    }
}
