namespace GlimpseTestApp.Core.Domain
{
    public class FileModel
    {
        public string Name { get; set; }
        public string Content { get; set; }

        public FileModel()
        {

        }

        public FileModel(string name, string content): this()
        {
            Name = name;
            Content = content;
        }
    }
}
