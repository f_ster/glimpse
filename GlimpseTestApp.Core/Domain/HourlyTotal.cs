using System;
using Newtonsoft.Json;

namespace GlimpseTestApp.Core.Domain
{
    public class HourlyTotal
    {
        [JsonProperty(PropertyName = "id")]
        public Guid Id { get; set; }

        public string Tag { get; set; }
        public int Hour { get; set; }
        public int Count { get; set; }

        public HourlyTotal()
        {
            Id = Guid.NewGuid();
        }

        public HourlyTotal(string tag, int hour):this()
        {
            Tag = tag;
            Hour = hour;
        }
    }
}
