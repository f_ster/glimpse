using System;
using Newtonsoft.Json;

namespace GlimpseTestApp.Core.Domain
{
    public class Image
    {
        [JsonProperty(PropertyName = "id")]
        public Guid Id { get; set; }
        public DateTimeOffset DateCreated { get; set; }
        public DateTimeOffset DateProcessed { get; set; }
        public string[] Tags { get; set; }

        public Image()
        {
            Tags = Array.Empty<string>();
        }

        public Image(DateTimeOffset dateCreated)
        {
            Id = Guid.NewGuid();
            DateCreated = dateCreated;
        }

        public void ApplyTags(string[] predictData)
        {
            Tags = predictData;
        }
    }
}
