namespace GlimpseTestApp.Core.Domain
{
    public class TagHourly
    {
        public int Hour { get; set; }
        public Tag[] Tags { get; set; }

        public class Tag
        {
            public Tag(string name, int count)
            {
                Name = name;
                Count = count;
            }

            public string Name { get; set; }
            public int Count { get; set; }
        }
    }
}
