﻿using System;
using GlimpseTestApp.Core.DomainServices;
using GlimpseTestApp.Core.Interfaces;
using GlimpseTestApp.Infrastructure.BlobStorage;
using GlimpseTestApp.Infrastructure.CosmosDb;
using GlimpseTestApp.Infrastructure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace GlimpseTestApp.Configuration
{
    public static class ApplicationBootstraps
    {
        public static IServiceCollection ConfigureProduction(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddConfiguration<AzureBusConfiguration>(configuration);
            services.AddConfiguration<CosmosConfiguration>(configuration);
            services.AddConfiguration<BlobContainerConfig>(configuration);

            services.AddSingleton<IBusProvider, BusProvider>();
            services.AddSingleton<IRepository, Repository>();
            services.AddSingleton<IStorageService, StorageService>();
            services.AddSingleton<IPredictionService, PredictionService>();
            services.AddSingleton<IImageProcessingCommandService, ImageProcessingCommandService>();
            services.AddSingleton<IImageProcessingQueryService, ImageProcessingQueryService>();

            return services;
        }

        public static IServiceCollection ConfigureTesting(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddConfiguration<AzureBusConfiguration>(configuration);
            services.AddConfiguration<CosmosConfiguration>(configuration);
            services.AddConfiguration<BlobContainerConfig>(configuration);

            services.AddSingleton<IBusProvider, BusProvider>();
            services.AddSingleton<IRepository, Repository>();
            services.AddSingleton<IStorageService, StorageService>();
            services.AddSingleton<IPredictionService, PredictionService>();
            services.AddSingleton<IImageProcessingCommandService, ImageProcessingCommandService>();
            services.AddSingleton<IImageProcessingQueryService, ImageProcessingQueryService>();

            return services;
        }

        private static void AddConfiguration<T>(this IServiceCollection services, IConfiguration configuration)
            where T : class
        {
            var instance = Activator.CreateInstance<T>();
            new ConfigureFromConfigurationOptions<T>(configuration.GetSection(typeof(T).Name)).Configure(instance);
            services.AddSingleton(instance);
        }
    }
}
