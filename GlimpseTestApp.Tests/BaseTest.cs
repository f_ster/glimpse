using System.Collections.Generic;
using GlimpseTestApp.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Memory;
using Microsoft.Extensions.DependencyInjection;

namespace GlimpseTestApp.Tests
{
    public class BaseTest
    {
        protected readonly ServiceProvider Container;

        protected BaseTest()
        {
            var memoryConfigurationSource = new MemoryConfigurationSource{InitialData = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("BlobContainerConfig:ContainerName","default"),
                new KeyValuePair<string, string>("CosmosConfiguration:DatabaseName","glimpse"),
                new KeyValuePair<string, string>("CosmosConfiguration:LeasesCollectionName","leases"),
                new KeyValuePair<string, string>("AzureBusConfiguration:Queue","glimpse"),
            }};
            Container = new ServiceCollection().ConfigureTesting(new ConfigurationRoot(new List<IConfigurationProvider>
            {
                new MemoryConfigurationProvider(memoryConfigurationSource)
            })).BuildServiceProvider();
        }
    }
}
