using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GlimpseTestApp.Core.Domain;
using GlimpseTestApp.Core.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace GlimpseTestApp.Tests
{
    public class ImageProcessingIntegrationTest : BaseTest
    {
        private readonly IImageProcessingCommandService _imageProcessingCommandService;

        public ImageProcessingIntegrationTest()
        {
            _imageProcessingCommandService = Container.GetService<IImageProcessingCommandService>();
            Container.GetService<IBusProvider>().StartListener(_imageProcessingCommandService.Process);
        }

        [Fact]
        public Task GenerateStats()
        {
            return _imageProcessingCommandService.GenerateStats();
        }

        [Fact]
        public async Task WholeImageProcessingCycle()
        {
            await PrepareEnumeration();
            var imageProcessingQueryService = Container.GetService<IImageProcessingQueryService>();
            var tags = await imageProcessingQueryService.SelectTagStatisticsAsync();
            await _imageProcessingCommandService.Upload("a1a1");
            //delay for bus, changefeed, ml
            Thread.Sleep(3000);

            var newTags = await imageProcessingQueryService.SelectTagStatisticsAsync();

            Assert.True(newTags.Sum(x => x.PredictionsCount) > tags.Sum(x => x.PredictionsCount));
        }

        private async Task PrepareEnumeration()
        {
            var repository = Container.GetService<IRepository>();
            await repository.Migrate(typeof(Image), typeof(Tag), typeof(HourlyTotal));
            repository.AddEnumerator<Image>(_imageProcessingCommandService.ActualizeStatistics, "tests");
            await repository.StartEnumerationAsync();
        }
    }
}
