using System.IO;
using System.Linq;
using GlimpseTestApp.Core.DomainServices;
using Xunit;

namespace GlimpseTestApp.Tests
{
    public class PredictionServiceTest
    {
        private readonly PredictionService _sut;

        public PredictionServiceTest()
        {
            _sut = new PredictionService();
        }

        [Fact]
        public void ParallelLimitReachingTest()
        {
            var memoryStream = new MemoryStream();
            var results = Enumerable.Range(0, 50).AsParallel().Select(x=>_sut.Predict(memoryStream));

            Assert.Contains(results, result => !result.IsSuccessful);
        }
    }
}
